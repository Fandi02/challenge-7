import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import HeaderComponent from '../component/HeaderComponent';
import OurServiceComponent from '../component/index/OurServiceComponent';
import WhyUsComponent from '../component/index/WhyUsComponent';
import TestimonialComponent from '../component/index/TestimonialComponent';
import ContainerComponent from '../component/index/ContainerComponent';
import FaqComponent from '../component/index/FaqComponent';
import FooterComponent from '../component/FooterComponent';

export default class Index extends Component {
  render() {
    return (
      <div>
        <HeaderComponent />
        <OurServiceComponent />
        <WhyUsComponent />
        <TestimonialComponent />
        <ContainerComponent />
        <FaqComponent />
        <FooterComponent />
      </div>

    )
  }
}