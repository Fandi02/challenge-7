import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import HeaderComponent from '../component/HeaderComponent';
import FooterComponent from '../component/FooterComponent';
import './SearchCar.css';
import axios from 'axios';
import { API_URL } from "../utils/constants";

export default class SearchCar extends Component {
  constructor(props) {
    super(props);
    this.state = { 
        carFilter: [],
     };
}

componentDidMount() {
    axios
        .get(API_URL + "filterCar")
        .then((response) => {
            const carFilter = response.data;
            this.setState({ carFilter });
            console.log(carFilter);
        })
        .catch((error) => {
            console.log(error);
        });
    }

  render() {
    return (
      <div>
        <HeaderComponent />
         <h1>
         <div className="filter shadow mb-5 bg-body row align-items-start">
              <div className="col">
                <label>
                  Tipe Driver
                  <select name="" id="">
                    <option value="" hidden>Pilih Tipe Driver</option> 
                    <option value="">Dengan Sopir</option>
                    <option value="">Tanpa Sopir (Lepas Kunci)</option>
                  </select>
                </label>
              </div>
              <div className="col">
                Tanggal <br />
                <input type="date" placeholder="Pilih Tanggal" />
              </div>
              <div className="col">
                Waktu Jemput <br />
                <input type="time" />
              </div>
              <div className="col">
                Jumlah Penumpang
                <input type="number" placeholder="Jumlah Penumpang" id="capacity" />
              </div>
              <div className="col">
                <button className="button" id="load-btn"><a href="/"> Cari Mobil </a></button>
                <button className="button" id="clear-btn" hidden> Clear Mobil </button>
              </div>
        </div>
         </h1>
         <FooterComponent />
      </div>
    )
  }
}
