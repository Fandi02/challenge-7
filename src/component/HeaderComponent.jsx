import React, { Component } from 'react';
import img from '../assets/car.png';
import './HeaderComponent.css';

export default class HeaderComponent extends Component {
  render() {
    return (
      <div>
          <header className="hero">
      <nav className="navbar navbar-light bg-light fixed-top navbar-expand-lg bg-opacity-50">
        <div className="container-fluid">
          <a href="/"><div className=" logo"></div></a>
          <button className="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasNavbar" aria-controls="offcanvasNavbar">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="offcanvas offcanvas-end" id="offcanvasNavbar" aria-labelledby="offcanvasNavbarLabel">
            <div className="offcanvas-header">
              <h5 className="offcanvas-title" id="offcanvasNavbarLabel"><a href="/"><div className="logo"></div></a></h5>
              <button type="button" className="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div className="offcanvas-body">
              <ul className="navbar-nav justify-content-end flex-grow-1 pe-3">
                <li className="nav-item">
                  <a className="nav-link text-dark" aria-current="page" href="#our-services">Our Services</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link text-dark" href="#why-us">Why Us</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link text-dark" href="#testimonial">Testimonial</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link text-dark" href="#faq">FAQ</a>
                </li>
                <li className="nav-item">
                  <button className="button"><a href="/">Register</a></button>
                </li>
            </ul>
            </div>
          </div>
        </div>
      </nav>

        <div className="container-fluid banner">
          <div className="row">
            <div className="col-sm-6 col-md-5 col-lg-6 column-satu px-5">
              <h1>Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)</h1>
              <p>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
              <button className="button"><a href='SearchCar'>Mulai Sewa Mobil</a></button>  
            </div>
            <div className="col-sm-6 col-md-5 col-lg-6 d-flex justify-content-end align-items-end">
              <img src={ img } alt="" className="bg position-absolute" />
            </div>
          </div>
        </div>
    </header>
      </div>
    )
  }
}
