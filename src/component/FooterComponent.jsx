import React, { Component } from 'react';
import './FooterComponent.css';
import iconFacebook from '../assets/icon_facebook.png';
import iconTwitter from '../assets/icon_twitter.png';
import iconInstagram from '../assets/icon_instagram.png';
import iconMail from '../assets/icon_mail.png';
import iconTwitch from '../assets/icon_twitch.png';

const styleObj = {
  margin: "0 3px"
}

export default class FooterComponent extends Component {
  render() {
    return (
      <div>
          <footer className="container">
            <div className="container text-md-start mt-5 pt-4">
                <div className="row mt-3">
                <div className="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4 text">
                <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                <p>binarcarrental@gmail.com</p>
                <p>081-233-334-808</p>
                </div>
                
                <div className="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4 link"> 
                <a href="#our-services"><p>Our services</p></a>
                <a href="#why-us"><p>Why Us</p></a>
                <a href="#testimonial"><p>Testimonial</p></a>
                <a href="#faq"><p>FAQ</p></a>
                </div>
                
                <div className="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4 text">
                <p>Connect with us</p>
                <div className="d-flex flex-row bd-highlight mb-3">
                    <img src={ iconFacebook } alt="" style={ styleObj } />
                    <img src={ iconInstagram } alt="" style={ styleObj } />
                    <img src={ iconTwitter } alt="" style={ styleObj } />
                    <img src={ iconMail } alt="" style={ styleObj } />
                    <img src={ iconTwitch } alt="" style={ styleObj } />
                </div>
                </div>
                
                <div className="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4 text">
                <p>Copyright Binar 2022</p>
                <a href=""><div className="logo"></div></a>
                </div>
                </div>   
            </div>
            </footer>
      </div>
    )
  }
}
