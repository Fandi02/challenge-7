import React, { Component } from 'react';
import imgService from '../../assets/img_service.png'
import imgGroup from "../../assets/Group 53.png";
import './OurServiceComponent.css';

const styleObj = {
  marginTop: "25px"  
}

export default class OurServiceComponent extends Component {
  render() {
    return (
      <div>
          <div id="our-services" className="container our-services">
            <div className="row">
              <div className="col-sm-6 col-md-5 col-lg-6">
                <img src={ imgService } alt="" />
              </div>
              <div className="col-sm-6 col-md-5 col-lg-6" style={styleObj}>
                <h2>Best Car Rental for any kind of trip in (Lokasimu)!</h2>
                      <p>Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain, kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.</p>
                      <ul>
                          <li><img src={imgGroup} alt="" /> Sewa Mobil Dengan Supir di Bali 12 Jam</li>
                          <li><img src={imgGroup} alt="" /> Sewa Mobil Lepas Kunci di Bali 24 Jam</li>
                          <li><img src={imgGroup} alt=""/> Sewa Mobil Jangka Panjang Bulanan</li>
                          <li><img src={imgGroup} alt="" /> Gratis Antar - Jemput Mobil di Bandara</li>
                          <li><img src={imgGroup} alt="" /> Layanan Airport Transfer / Drop In Out</li>
                      </ul>
              </div>
            </div>
          </div>
      </div>
    )
  }
}
