import React, { Component } from 'react';
import img_photo from '../../assets/img_photo.png';
import img_photo_1 from '../../assets/img_photo_1.png';
import Rate from '../../assets/Rate.png';
import './TestimonialComponent';
import leftButton from '../../assets/Left button.png';
import rightButton from '../../assets/Right button.png';

const styleObjSatu = {
    marginTop: "25px"
},
styleObjDua = {
    marginRight: "25px",
}

export default class TestimonialComponent extends Component {
  render() {
    return (
      <div>
          <section id="testimonial" className="container testimonial">
            <center>
                <h2>Testimonial</h2>
                <p>Berbagai review positif dari para pelanggan kami</p>
            </center>
            <div id="carouselExampleIndicators" className="carousel slide" data-bs-ride="carousel">
                <div className="carousel-inner">
                <div className="carousel-item active">
                    <div className="row">
                    <div className="col-sm-6">
                        <div className="card">
                        <div className="card-body">
                            <div className="row">
                            <div className="col-3 d-flex align-items-center">
                                <img src={ img_photo } alt="" />
                            </div>
                            <div className="col">
                                <img src={ Rate } alt="" />
                                <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                                <b>John Dee 32, Bromo</b>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className="card">
                        <div className="card-body">
                            <div className="row">
                            <div className="col-3 d-flex align-items-center">
                                <img src={img_photo_1} alt="" />
                            </div>
                            <div className="col">
                                <img src={ Rate } alt="" />
                                <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                                <b>John Dee 32, Bromo</b>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div className="carousel-item">
                    <div className="row">
                    <div className="col-sm-6">
                        <div className="card">
                        <div className="card-body">
                            <div className="row">
                            <div className="col-3 d-flex align-items-center">
                                <img src={ img_photo } alt="" />
                            </div>
                            <div className="col">
                                <img src={ Rate } alt="" />
                                <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                                <b>John Dee 32, Bromo</b>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className="card">
                        <div className="card-body">
                            <div className="row">
                            <div className="col-3 d-flex align-items-center">
                                <img src={img_photo_1} alt="" />
                            </div>
                            <div className="col">
                                <img src={ Rate } alt="" />
                                <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                                <b>John Dee 32, Bromo</b>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
                <div className="d-flex justify-content-center" style={ styleObjSatu }>
                <a href="#carousel-example-generic" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev"><img src={ leftButton } alt="" style={ styleObjDua } /></a>
                <a href="#carousel-example-generic" data-bs-target="#carouselExampleIndicators" data-bs-slide="next"><img src={ rightButton } alt="" /></a>
                </div>
            </div>
            </section>
      </div>
    )
  }
}
