import React, { Component } from 'react';
import './ContainerComponent.css';

export default class ContainerComponent extends Component {
  render() {
    return (
      <div>
          <section className="container">
            <div className="card-body cta-banner">
                <h1 className="card-title">Sewa Mobil di (Lokasimu) Sekarang</h1>
                <p className="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                <button className="button"><a href="">Mulai Sewa Mobil</a></button>
            </div>
            </section>
      </div>
    )
  }
}
