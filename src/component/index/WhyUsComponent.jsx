import React, { Component } from 'react';
import icon_complete from '../../assets/icon_complete.png';
import icon_price from '../../assets/icon_price.png';
import icon_24hrs from '../../assets/icon_24hrs.png';
import icon_professional from '../../assets/icon_professional.png';
import './WhyUsComponent.css';

const styleObj = {
    margin: "0px 10px",
}

export default class WhyUsComponent extends Component {
  render() {
    return (
      <div>
          <section id="why-us" className="container why-us">
            <h2>Why Us?</h2>
            <p>Mengapa harus pilih Binar Car Rental?</p>
            <div className="row row-cols-1 row-cols-md-4 g-3">
            <div className="col">
                <div className="card h-100" style={styleObj}>
                <img src={icon_complete} className="card-img-top" alt="..." />
                <div className="card-body">
                    <h5 className="card-title">Mobil Lengkap</h5>
                    <p className="card-text">Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                </div>
                </div>
            </div>
            <div className="col">
                <div className="card h-100" style={styleObj}>
                <img src={ icon_price } className="card-img-top" alt="..." />
                <div className="card-body">
                    <h5 className="card-title">Harga Murah</h5>
                    <p className="card-text">Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain</p>
                </div>
                </div>
            </div>
            <div className="col">
                <div className="card h-100" style={styleObj}>
                <img src={ icon_24hrs} className="card-img-top" alt="..." />
                <div className="card-body">
                    <h5 className="card-title">Layanan 24 Jam</h5>
                    <p className="card-text">Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu</p>
                </div>
                </div>
            </div>
            <div className="col">
                <div className="card h-100" style={styleObj}>
                <img src={ icon_professional } className="" alt="..." />
                <div className="card-body">
                    <h5 className="card-title">Sopir Professional</h5>
                    <p className="card-text">Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu</p>
                </div>
                </div>
            </div>
            </div>
        </section>
      </div>
    )
  }
}
