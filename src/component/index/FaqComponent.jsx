import React, { Component } from 'react';
import './FaqComponent.css';

export default class FaqComponent extends Component {
  render() {
    return (
      <div>
          <section id="faq" className="container faq">
            <div className="row">
                <div className="col-sm-6 col-md-5 col-lg-6">
                <h3>Frequently Asked Question</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                </div>
                <div className="col-sm-6 col-md-5 col-lg-6">
                <div className="accordion accordion-flush" id="accordionFlushExample">
                    <div className="accordion-item">
                    <h2 className="accordion-header" id="flush-headingOne">
                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                            Apa saja syarat yang dibutuhkan?
                        </button>
                    </h2>
                    <div id="flush-collapseOne" className="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                        <div className="accordion-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas architecto aliquam aut tempora? Vel nihil quod, voluptate recusandae numquam totam ullam consectetur voluptas obcaecati ea odit facilis iste eligendi expedita.</div>
                    </div>
                    </div>
                    <div className="accordion-item">
                    <h2 className="accordion-header" id="flush-headingTwo">
                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                            Berapa hari minimal sewa mobil lepas kunci?
                        </button>
                    </h2>
                    <div id="flush-collapseTwo" className="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                        <div className="accordion-body">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum ex accusantium consequatur, ullam a, necessitatibus provident quam itaque quidem ab labore ducimus, perferendis non? A sit soluta fuga nostrum distinctio.</div>
                    </div>
                    </div>
                    <div className="accordion-item">
                    <h2 className="accordion-header" id="flush-headingThree">
                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                            Berapa hari sebelumnya sabaiknya booking sewa mobil?
                        </button>
                    </h2>
                    <div id="flush-collapseThree" className="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                        <div className="accordion-body">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem maiores omnis rerum possimus officia quas saepe ab aspernatur laudantium! Reprehenderit necessitatibus tempora temporibus. Ea dolorem itaque, corrupti pariatur modi labore?</div>
                    </div>
                    </div>
                    <div className="accordion-item">
                        <h2 className="accordion-header" id="flush-headingFour">
                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour" aria-expanded="false" aria-controls="flush-collapseFour">
                            Apakah Ada biaya antar-jemput?
                        </button>
                        </h2>
                        <div id="flush-collapseFour" className="accordion-collapse collapse" aria-labelledby="flush-headingFour" data-bs-parent="#accordionFlushExample">
                        <div className="accordion-body">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi eius praesentium numquam, sapiente alias et accusantium commodi nemo ab magni. Alias, veniam debitis dolore minima consequuntur laborum ut modi iusto!</div>
                        </div>
                    </div>
                    <div className="accordion-item">
                        <h2 className="accordion-header" id="flush-headingFive">
                        <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFive" aria-expanded="false" aria-controls="flush-collapseFive">
                            Bagaimana jika terjadi kecelakaan
                        </button>
                        </h2>
                        <div id="flush-collapseFive" className="accordion-collapse collapse" aria-labelledby="flush-headingFive" data-bs-parent="#accordionFlushExample">
                        <div className="accordion-body">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores nisi ab repellendus inventore, ducimus dolorem magni maxime nam ipsa quas id blanditiis at doloribus assumenda quis. Saepe impedit voluptas sit!</div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            </section>
      </div>
    )
  }
}
